<?php

/**
 * Sessioner
 * https://codeberg.org/jrm-omg/session
 */
class Sessioner {

    const version = '0.0.1';
    
    /**
     * Session init
     */
    public function __construct() {
        if (session_name() !== 'sessioner') {
            session_name('sessioner');
            session_set_cookie_params([
                'httponly' => true,
                'samesite' => 'Strict'
            ]);
        }
        session_start();
    }
    
    /**
     * Session delete
     */
    public function delete() {
        if (empty($_SESSION) === false) {
            $_SESSION = [];
        }
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time()-1, '/');
        }
        session_destroy();
    }
    
    /**
     * Create an authenticated session
     */
    function authenticate()
    {
        $_SESSION = [
            'auth' => true,
            'created_at' => time()
        ];
    }
    
    /**
     * Check for an authenticated session
     *
     * @return bool True if the session is authenticated
     */
    function authenticated()
    {
        return ( isset($_SESSION['auth']) && $_SESSION['auth'] === true );
    }
  
}