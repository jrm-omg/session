# session

quick repo to store stuffs about PHP sessions

## usage

Auth check

```php
require __DIR__ . '/Sessioner.php';
$s = new Sessioner;
if ($s->authenticated()) {
  echo 'you are authenticated';
} else  {
  echo 'you are NOT authenticated';
}
```

Auth create

```php
$userPassword = $_POST['password'] ?? false;

if ($userPassword === 'omg this password is so secure') {
  $s->authenticate();
  header('Location: index.php');
  exit;
}
```

Logout

```php
$s = new Sessioner;
$s->delete(); // logout
```
